using System;

public class Test
{
	public static void Main()
	{
		double X = Convert.ToDouble(Console.ReadLine());
		int N = Convert.ToInt32(Console.ReadLine());
		double s=1.0;
		double power = 1.0;
		int fact = 1;
		for (int i=1; i<N+1;i++) {
			s=s+X*power/fact;
			power=power*X;
			fact=fact*(i+1);
		}
		Console.WriteLine(s);
	}
}
