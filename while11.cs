using System;

public class Test
{
	public static void Main()
	{
		int N = Convert.ToInt32(Console.ReadLine());
		int s=1;
		int c=2;
		while (s<N) {
			s=s+c;
			c=c+1;
		}
		Console.WriteLine($"{s} {c-1}");
	}
}
