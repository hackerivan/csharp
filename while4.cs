using System;

public class Test
{
	public static void Main()
	{
		int N = Convert.ToInt32(Console.ReadLine());
		while (N%3==0) {
			N=N/3;
		}
		if (N==1) {
			Console.WriteLine(true);
		} else {
			Console.WriteLine(false);
		}
	}
}
