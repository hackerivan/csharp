using System;

public class Test
{
	public static void Main()
	{
		int N = Convert.ToInt32(Console.ReadLine());
		int s = N*N;
		for (int i=1 ; i<N+1; i++) {
			s=s+(N+i)*(N+i);
		}
		Console.WriteLine(s);
	}
}
