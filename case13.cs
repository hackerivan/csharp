using System;

public class Test
{
	public static void Main()
	{
		int n = Convert.ToInt32(Console.ReadLine());
		double x = Convert.ToDouble(Console.ReadLine());
		switch (n) {
			case 1: Console.WriteLine($"{x*Math.Pow(2, 0.5)} {x*Math.Pow(2, 0.5)/2} {x*Math.Pow(2, 0.5)*(x*Math.Pow(2, 0.5)/2)/2}"); break;
			case 2: Console.WriteLine($"{x/Math.Pow(2, 0.5)} {x/2} {x*x/4}"); break;
			case 3: Console.WriteLine($"{x*2/Math.Pow(2, 0.5)} {x*2} {x*x}"); break;
			case 4: Console.WriteLine($"{Math.Pow(x, 0.5)*2/Math.Pow(2, 0.5)} {Math.Pow(x, 0.5)*2} {Math.Pow(x, 0.5)}"); break;
		}
	}
}
