using System;

public class Test
{
	public static void Main()
	{
		int N = Convert.ToInt32(Console.ReadLine());
		if (N%2==0) {
			if (N==2) {
				Console.WriteLine(true);
			} else {
				Console.WriteLine(false);
			}
		} else {
			int c =2;
			while (!(N%c==0 || c*c>N)) {
				c+=1;
			}
			if (c*c>N) {
				Console.WriteLine(true);
			} else {
				Console.WriteLine(false);
			}
		}
	}
}
