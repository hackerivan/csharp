using System;

public class Test
{
	public static void Main()
	{
		double x =Convert.ToDouble(Console.ReadLine());
		int y = (int)x;
		if (x<0) {
			Console.WriteLine(0);
		} else if (y%2==0) {
			Console.WriteLine(1);
		} else if (y%2!=0) {
			Console.WriteLine(-1);
		}
	}
}
