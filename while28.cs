using System;

public class Test
{
	public static void Main()
	{
		double E = Convert.ToDouble(Console.ReadLine());
		double A1=2.0;
		int c = 2;
		double A2= 2+1/A1;
		while (Math.Abs(A2-A1)>E) {
			c+=1;
			A1=A2;
			A2=2+1/A1;
		}
		Console.WriteLine($"{c} {A1} {A2}");
		
	}
}
